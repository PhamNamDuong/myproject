/**
 * Quy trình thực hiện :
 *
 * 1. lấy danh sách dữ sản phẩm từ api
 *    - cần: +, 1 hàm fetch dữ liệu từ api
 *           +, lấy xong phải hiển thị -> 1 hàm hiển thị sản phẩm (renderShop)
 *
 * 2. thêm chức năng lọc
 *    - cần: +, code 1 button lọc
 *           +, code hàm xử lí event lọc
 *           +, hàm hiển thị sản phẩm lọc (renderShop) : đã có
 * 3. thêm chức năng add to cart
 *    - cần: +, 1 hàm call back xử lí sụ kiện add to cart
 *           +, 1 hàm renderCart
 * 4. thêm chức năng tăng giảm số lượng
 * 5. thêm chức năng xoá sản phẩm từ giỏ hàng
 * 6. lưu dữ liệu vào local storage
 *
 */
/* --------------------------------------- Global Variable ------------------------------------ */

// const BASE_URL = "https://mock-electronic-server.herokuapp.com/api";
let productList = [];
let carts = [];

const formatVND = (num) => {
   return new Intl.NumberFormat("vi-VN", {
      style: "currency",
      currency: "VND",
   }).format(num);
};

/*--------------------------------------- Select element from DOM ----------------------------- */

const renderShop = document.querySelector(".renderShop");
const filterByType = document.querySelector(".filter-by-type");
const btnCart = document.getElementById("btnCart");
const totalPrice = document.getElementById("printMoney");
const tableCart = document.getElementById("tbodySanPham");
const btnRemoveItem = document.getElementById("btnRemoveItem");
const btnTotal = document.getElementById("btnTotal");

/* ------------------------------------- Render UI ------------------------------------------- */

function renderListProduct(listProducts) {
   listProducts.forEach((product) => {
      renderShop.innerHTML += `
        <div class="col">
            <div class="item item-1">
                <img src= ${product.img} alt="" />
                <div class="info">
                    <p>${product.name}</p>
                    <button class="btn-cart btn-success" id="btnCart" value = ${product.id}>cart</button>
                </div>
            </div>
        </div>
        `;
   });
}

function renderCart(carts) {
   carts.forEach((item) => {
      tableCart.innerHTML += `
      <tr>
         <th class="product-thumbnail">
            <a href="#" className="product-link">
            <img src=${item.image} alt=""váy" width="100px" />
            </a>
         </th>

         <th class="product-name">
            <a href="#">${item.name}</a>
         </th>
         <th class="product-price">
            <span>${formatVND(item.price)}</span>
         </th>

         <th class="product-quantity">
            <div class="flex">
               <div class="shopcart-plus-minus">
               <button class="decrease" id ="decrease" value=${item.id}>
               -
               </button>
               <input type="text" class="quantity-box" name="qtybutton" value=${
                  item.quantity
               } readOnly />
               <button class="increase" id ="increase" value=${item.id}>
                  +
               </button>
            </div>
   
            <i class="fa-solid fa-trash right ${
               item.id
            }" class="" id="btnRemoveItem" ></i>
                
            </div>

         </th>

         <th class="product-subtotal">
            <span class="subtotal">${formatVND(
               item.price * item.quantity
            )}</span>
         </th>
    </tr>
      `;
   });
}

function renderTotalPrice(carts) {
   // cập nhật tổng tièn:
   const value = carts.reduce((acc, item) => {
      return acc + item.price * item.quantity;
   }, 0);
   totalPrice.innerHTML = formatVND(value);
}
/*-------------------------------------------- Event Handler ----------------------------------- */

document.querySelector("body").addEventListener("click", (event) => {
   // add to cart event hanlder
   if (event.target.id === "btnCart") {
      // lấy sản phẩm đang được chọn
      const [currProduct] = productList.filter(
         (product) => product.id === event.target.value
      );

      console.log(currProduct);
      //: nếu chưa có trong giỏ hàng (carts): push vào cart
      //: nếu sp đã có: tăng số lượng (quantity) + 1

      // cách viết 1:
      const alreadyInCart = carts.find((item) => item.id === currProduct.id);

      alreadyInCart
         ? carts.forEach((item) => {
              item.quantity =
                 item.id === currProduct.id
                    ? (item.quantity += 1)
                    : item.quantity;
           })
         : carts.push({
              id: currProduct.id,
              name: currProduct.name,
              price: currProduct.price,
              quantity: 1,
              image: currProduct.img,
           });

      // console.log(carts)
      // render lại carts

      // - setItem
      // - getItem
      // - clear
      // setItem: 2params: 1, key; 2: value (JSON)
      localStorage.setItem("carts", JSON.stringify(carts));
      tableCart.innerHTML = "";
      renderCart(carts);
      renderTotalPrice(carts);
      window.alert("Đã thêm sản phẩm vào giỏ hàng");
   }
});

// sự kiện onchange filter
filterByType.addEventListener("change", (e) => {
   //    console.log(e.target.value);
   const option = e.target.value;
   let filteredProducts = [];
   let isFiltered = false;

   if (option !== "all" && option !== "") {
      isFiltered = true;
      filteredProducts = productList.filter(
         (product) => product.type === option
      );
   }

   renderShop.innerHTML = "";
   isFiltered
      ? renderListProduct(filteredProducts)
      : renderListProduct(productList);
});

document.querySelector("body").addEventListener("click", (e) => {
   // giảm cart
   if (e.target.id === "decrease") {
      carts.forEach((item) => {
         item.id === e.target.value
            ? item.quantity === 1
               ? 1
               : (item.quantity -= 1)
            : null;
      });

      localStorage.setItem("carts", JSON.stringify(carts));
      tableCart.innerHTML = "";
      renderCart(carts);
      renderTotalPrice(carts);
   }
});

// tăng số lượng
document.querySelector("body").addEventListener("click", (e) => {
   if (e.target.id === "increase") {
      carts.forEach((item) => {
         item.id === e.target.value ? (item.quantity += 1) : null;
      });

      localStorage.setItem("carts", JSON.stringify(carts));
      tableCart.innerHTML = "";
      renderCart(carts);
      renderTotalPrice(carts);
   }
});

document.querySelector("body").addEventListener("click", (e) => {
   if (e.target.id === "btnRemoveItem") {
      if (confirm("Xác nhận xoá sản phẩm này khỏi giỏ hàng?") === true) {
         // e.target.className =>  "fa-solid fa-trash right 6"
         // e.target.className.split(" ") => ["fa-solid", "fa-trash", "right", "6"]
         // e.target.className.split(" ").pop() = "6"
         const currID = e.target.className.split(" ").pop(); // vd = 1

         //carts.filter((item) => item.id !== Number.parseInt(currID))
         // -> [{id: 2, name: "bbb"},  {id: 3, name: "cc"},]
         /**
          * [
          *    {id: 1, name: "aaaa"},
          *    {id: 2, name: "bbb"},
          *    {id: 3, name: "cc"},
          * ]
          */
         // carts ---- forEach
         carts = [...carts.filter((item) => item.id !== currID)];

         localStorage.setItem("carts", JSON.stringify(carts));
         tableCart.innerHTML = "";
         renderCart(carts);
         renderTotalPrice(carts);
      }
   }
});

btnTotal.addEventListener("click", () => {
   alert("Giỏ hàng đã được thanh toán!");
   tableCart.innerHTML = "";
   carts = [];
   localStorage.clear();
   renderCart(carts);
   renderTotalPrice(carts);
});
/* ----------------------------------------------- Fetch Api ------------------------------------- */

axios({
   method: "get",
   url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
})
   .then(function (response) {
      const products = response.data;
      productList = [...products];
      renderShop.innerHTML = "";
      renderListProduct(productList);
   })
   .catch(function (err) {
      console.error(err);
   });

carts = JSON.parse(localStorage.getItem("carts"));
// nếu cart k rỗng thì update lên giao diện, ngược lại khởi trị bẵng mảng rỗng
if (carts) {
   renderCart(carts);
   renderTotalPrice(carts);
} else {
   carts = [];
}

// async function getAllProducts() {
//    try {
//       const response = await axios.get(
//          `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products`
//       );
//       return response.data;
//    } catch (err) {
//       console.error(err);
//    }
// }
// getAllProducts().then((products) => {
//    arr1 = [1, 2, 3];
//    (arr2 = []),
//       // arr1.forEach(item => {
//       //    arr2.push(item)
//       // })

//       (productList = [...products]);
//    console.log(productList);
//    renderShop.innerHTML = "";
//    renderListProduct(productList);
// });
/* --------------------------------- Main ---------------------------*/
// getItem: 1pram key
// parse: đổi từ Json -> kiểu thông thường

// get 1 sản phẩm:
// let id = 4
// const data = await axios.get(`https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`)
// post

// let newProduct ={
//    id: 10,
//    náme: "Iphone",
//    image: "......",
//    price: 1111
// }
