const formatVND = (num) => {
   return new Intl.NumberFormat("vi-VN", {
      style: "currency",
      currency: "VND",
   }).format(num);
};

/** --------------------------------------- GLOBAL VARIABLES ------------------------------*/

let products = [];

/** --------------------------------------- SELECTOR FROM DOM -----------------------------*/

const btnAdd = document.getElementById("btnAdd");
const formAction = document.getElementById("product-action");
const productsTable = document.getElementById("productsTable");
const productDetail = document.querySelector(".product-detail");

/** --------------------------------------- RENDER UI ------------------------------------ */
const renderProducts = (products) => {
   products.forEach((product) => {
      productsTable.innerHTML += `
        <tr>
            <td>${product.id}</td>
            <td class="img-container">
                <img src=${product.img} alt="">
            </td>
            <td>${product.name}</td>

            <td>${formatVND(product.price)}</td>
            <td>${product.quantity ? product.quantity : 0}</td>
            <td class="action">
                <i class='bx bx-trash ${product.id}' id ="delete"></i>
                <i class='bx bx-edit ${product.id}' id ="update"></i>
                <i class='bx bx-info-circle ${product.id}' id="detail"></i>
            </td>
        </tr>
        `;
   });

   document.getElementById(
      "productCounter"
   ).innerText = `Tổng số ${products.length} sản phẩm`;
};

const renderProductDetails = (product) => {
   productDetail.classList.remove("hidden");
   productDetail.innerHTML += `
      <h2 class="dash-title">Chi tiết sản phẩm</h2>
      <div class="row">
         <div class="col-lg-5 col-sm-12 center">
            <div class="img-wrapper">
               <img src=${product.img} alt="" height="100%" width="100%" />
            </div>
         </div>

         <div class="col-lg-7 col-sm-12 prod-wrapper">
            <h3 class="prod-title">${product.name}</h3>
            <div class="product__desc">
               <span>Mô tả: </span>
               <span>${product.desc}</span>
            </div>
            <div className="prodcut-details__price">
               <span>Giá tiền</span>
               <span>${formatVND(product.price)}</span>
            </div>
            <div class="frontCamera">
               <span>Camera trước: </span>
               <span>${product.frontCamera}</span>
            </div>
            <div class="backCamera">
               <span>Camera sau: </span>
               <span>${product.backCamera}</span>
            </div>
            <div class="screen">
               <span>Màn hình: </span>
               <span>${product.screen}</span>
            </div>
            <div class="product__type">
               <span>Hãng sản xuất: </span>
               <span>${product.type}</span>
            </div>
            <div class="product__quantity">
               <span>Số lượng trong kho: </span>
               <span>${product.quantity}</span>
            </div>
            <div class="btn-area" style="width: 30%">
               <button class="btn-cancel" id="btnClose">Đóng</button>
            </div>
      </div>
   </div>
   `;
};

const renderForm = (product = undefined) => {
   const btnText = product ? "Cập&nbsp;nhật" : "Thêm";
   const name = product ? product.name.split(" ").join("&nbsp;") : "";
   // console.log(name);
   const price = product ? product.price : "";
   const screen = product ? product.screen.split(" ").join("&nbsp;") : "";
   const frontCamera = product
      ? product.frontCamera.split(" ").join("&nbsp;")
      : "";
   const backCamera = product
      ? product.backCamera.split(" ").join("&nbsp;")
      : "";
   const img = product ? product.img : "";
   const desc = product ? product.desc : "";
   const type = product ? product.type : "";
   const quantity = product ? product.quantity : "";
   formAction.innerHTML = "";
   formAction.classList.remove("hidden");
   const title = product ? "Chỉnh sửa thông tin sản phẩm" : "Thêm sản phẩm";

   formAction.innerHTML = `
   <h2 class="dash-title">${title}</h2>
   <form class="product-add__form" id="product-add__form">
       <div class="form-left">
           <div class="product-add flex">
               <label for="name">Tên sản phẩm </label>
               <input type="text" id="name" value= ${name}>
           </div>
           <div class="product-add flex">
               <label for="price">Giá</label>
               <input type="text" id="price" value = ${price}>
           </div>
           <div class="product-add flex">
               <label for="screen">Màn hình</label>
               <input type="text" id="screen" value = ${screen}>
           </div>
           <div class="product-add flex">
               <label for="frontCamera">Camera trước</label>
               <input type="text" id="frontCamera" value = ${frontCamera}>
           </div>
           <div class="product-add flex">
               <label for="backCamera">Camera sau</label>
               <input type="text" id="backCamera" value = ${backCamera}>
           </div>

       </div>
       <div class="form-right">

           <div class="product-add flex">
               <label for="img">Link ảnh</label>
               <input type="text" id="img" value = ${img}>
           </div>
           <div class="product-add__desc flex">
               <label for="desc">Mô tả</label>
               <textarea name="" id="desc" value= >${desc}</textarea>
           </div>

           <div class="product-add flex">
               <label for="type">Loại sản phẩm</label>
               <input type="text" id="type" value = ${type}>
           </div>
           <div class="product-add flex">
               <label for="quantity">Số lượng</label>
               <input type="text" id="quantity" value = ${quantity}>
           </div>
           <div class="btn-area">
               <button class="btn-cancel" id="btnCancel">Huỷ</button>
               <input type="submit" class="add-product ${
                  product ? product.id : ""
               }" id="btnSubmit" value = ${btnText} >
           </div>
       </div>

   </form>
   `;
};
/** --------------------------------------- EVENT HANDLER -------------------------------- */

// mở form thêm sản phẩm
document.querySelector("body").addEventListener("click", (e) => {
   if (e.target.id === "btnAdd") {
      renderForm();
   }
});

// mở form update sản phẩm
document.querySelector("body").addEventListener("click", (e) => {
   if (e.target.id === "update") {
      const id = e.target.parentNode.parentNode.childNodes[1].innerHTML;
      const [currProduct] = products.filter((item) => item.id === id);
      console.log(currProduct);
      renderForm(currProduct);
   }
});

// event thêm / sửa
document.querySelector("body").addEventListener("click", (e) => {
   if (e.target.id === "btnSubmit") {
      e.preventDefault();
      // lấy form hiện tại
      let form = document.getElementById("product-add__form").elements;

      console.log(form);
      let data = {};
      // duyệt qua form để lấy dữ liệu
      for (let i = 0; i < form.length; i++) {
         if (form[i].id !== "btnSubmit" && form[i].id !== "btnCancel")
            data[`${form[i].id}`] = form[i].value;
      }
      console.log(data);
      // gọi api thêm sản phẩm
      if (data.name === "" || data.price === "" || data.img === "") {
         alert("Bạn chưa điền đầy đủ thông tin sản phẩm");
      } else {
         // kiểm tra form là thêm hay là chỉnh sửa
         const arr = e.target.classList;
         console.log(arr);
         if (arr.length > 1) {
            const id = arr[1];
            updateProduct(id, data);
         } else {
            addProduct(data);
         }
      }
   }
});

// đóng form thêm / sửa
document.querySelector("body").addEventListener("click", (e) => {
   if (e.target.id === "btnCancel") {
      formAction.innerHTML = "";
      formAction.classList.add("hidden");
   }
});

// mở thông tin sản phẩm
document.querySelector("body").addEventListener("click", (e) => {
   if (e.target.id === "detail") {
      console.log(e.target.parentNode.parentNode.childNodes);
      const id = e.target.parentNode.parentNode.childNodes[1].innerHTML;
      getSingleProduct(id);
   }
});

// đóng thông tin sản phẩm
document.querySelector("body").addEventListener("click", (e) => {
   if (e.target.id === "btnClose") {
      productDetail.classList.add("hidden");
      productDetail.innerHTML = "";
   }
});

// event xoá sản phẩm
document.querySelector("body").addEventListener("click", (e) => {
   if (e.target.id === "delete") {
      const id = e.target.parentNode.parentNode.childNodes[1].innerHTML;
      const id1 = e.target.classList[e.target.classList.length - 1];
      console.log(id1);
      // console.log(id);
      if (confirm("Bạn chắc chắn muốn xoá sản phẩm?") === true) {
         deleteProduct(id);
      }
   }
});

/**---------------------------------------- FETCH API ----------------------------------- */
// lấy danh sách sản phẩm

const fetchAllProduct = () => {
   axios({
      method: "GET",
      url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
   })
      .then((response) => {
         products = [...response.data];
         // console.log(products);
         productsTable.innerHTML = "";
         renderProducts(products);
         let keyword = "Samsung";
         const result = products.filter((product) => {
            return product.name.includes(keyword);
         });
         console.log(result);
      })
      .catch((error) => {
         console.error(error);
         alert("Fetch dữ liệu thất bại!");
      });
};

// thêm sản phẩm
const addProduct = (data) => {
   axios({
      method: "POST",
      url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
      data: {
         name: data.name,
         price: data.price,
         screen: data.screen,
         backCamera: data.backCamera,
         frontCamera: data.frontCamera,
         img: data.img,
         desc: data.desc,
         type: data.type,
         quantity: Number(data.quantity),
      },
   })
      .then((response) => {
         console.log(response);
         alert("Đã thêm sản phẩm thành công!");
         fetchAllProduct();
      })
      .catch((err) => {
         console.log(err);
         alert("Thêm sản phẩm thất bại!");
      });
};

// lấy thông tin 1 sản phẩm
const getSingleProduct = (id) => {
   axios({
      method: "GET",
      url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
   })
      .then((response) => {
         console.log(response);
         productDetail.innerHTML = "";
         renderProductDetails(response.data);
      })
      .catch((err) => {
         console.log(err);
         alert(`Không tìm thấy sản phẩm có id ${id}`);
      });
};

// cập nhật thông tin sản phẩm
const updateProduct = (id, data) => {
   axios({
      method: "PUT",
      url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
      data: {
         name: data.name,
         price: data.price,
         screen: data.screen,
         backCamera: data.backCamera,
         frontCamera: data.frontCamera,
         img: data.img,
         desc: data.desc,
         type: data.type,
         quantity: Number(data.quantity),
      },
   })
      .then((response) => {
         console.log(response);
         alert("Đã cập nhật thông tin sản phẩm");
         fetchAllProduct();
      })
      .catch((err) => {
         console.log(err);
      });
};

const deleteProduct = (id) => {
   axios({
      method: "DELETE",
      url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
   })
      .then((res) => {
         console.log(res);
         alert("Đã xoá sản phẩm thành công!");
         fetchAllProduct();
      })
      .catch((err) => {
         console.log(err);
         alert("Không thể xoá sản phẩm!");
      });
};
/**---------------------------------------- MAIN--- ----------------------------------- */
fetchAllProduct();
